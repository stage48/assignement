package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.requests.VersementRequest;
import ma.octo.assignement.requests.VirementRequest;
import ma.octo.assignement.responses.VersementResponse;
import ma.octo.assignement.responses.VirementResponse;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api/v1/banque")

class BanqueController {

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private VirementRepository virementRepository;
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private VersementService versementService;

    @Autowired
    private VirementService virementService;

    @GetMapping("/lister_virements")
    ResponseEntity<List<Virement>> loadAll() {
        List<Virement> all = virementRepository.findAll();
        return new ResponseEntity<List<Virement>>(all , HttpStatus.OK);
    }

    @GetMapping("/lister_comptes")
    ResponseEntity<List<Compte>>  loadAllCompte() {
        List<Compte> all = compteRepository.findAll();
        return new ResponseEntity<List<Compte>>(all , HttpStatus.OK);
    }

    @GetMapping("/lister_utilisateurs")
    ResponseEntity<List<Utilisateur>> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurRepository.findAll();
        return new ResponseEntity<List<Utilisateur>>(all , HttpStatus.OK);
    }

    @PostMapping("/virement")
    public ResponseEntity<VirementResponse> virement(@RequestBody VirementRequest virementRequest)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
      VirementDto virementDto = new VirementDto();
      BeanUtils.copyProperties(virementRequest, virementDto);
      VirementDto virementEffectuer = virementService.virement(virementDto) ;
      VirementResponse virementResponse = new VirementResponse();
      BeanUtils.copyProperties(virementEffectuer, virementResponse); 
      return new ResponseEntity<VirementResponse>(virementResponse , HttpStatus.CREATED);
    }

    @PostMapping("/versement")
    public ResponseEntity<VersementResponse> versement(@RequestBody VersementRequest versementRequest) throws CompteNonExistantException{

        VersementDto versementDto = new VersementDto();
        BeanUtils.copyProperties(versementRequest,versementDto);
         
        VersementDto versementDtoEffectuer = versementService.versement(versementDto);
        VersementResponse versementResponse = new VersementResponse();
        
        BeanUtils.copyProperties(versementDtoEffectuer,versementResponse);
        
        return new ResponseEntity<VersementResponse>(versementResponse , HttpStatus.CREATED);
    }
}
