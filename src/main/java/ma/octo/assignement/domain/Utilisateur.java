package ma.octo.assignement.domain;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(
  name = "UTILISATEUR",
  uniqueConstraints = {
    @UniqueConstraint(name = "utilisateur_username_unique", columnNames = "username")
  }
)
public class Utilisateur implements Serializable {
  @Id
  @SequenceGenerator(
			name = "utilisateur_sequence",
			sequenceName = "utilisateur_sequence",
			allocationSize = 1
	)
	
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "utilisateur_sequence"
	)
  private Long id;

  @Column(length = 10, nullable = false)
  private String username;

  @Column(length = 10, nullable = false)
  private String gender;

  @Column(length = 60, nullable = false)
  private String lastname;

  @Column(length = 60, nullable = false)
  private String firstname;

  @Temporal(TemporalType.DATE)
  private Date birthdate;


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public Date getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(Date birthdate) {
    this.birthdate = birthdate;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Long getId() {
    return id;
  }

}
