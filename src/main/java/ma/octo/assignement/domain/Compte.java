package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(
  name = "COMPTE",
  uniqueConstraints = {
    @UniqueConstraint(name = "compte_nrCompte_unique", columnNames = "nrCompte")
  }
)
public class Compte {
  @Id
  @SequenceGenerator(
			name = "compte_sequence",
			sequenceName = "compte_sequence",
			allocationSize = 1
	)
	
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "compte_sequence"
	)
  private Long id;

  @Column(length = 16)
  private String nrCompte;

  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal solde;

  @ManyToOne()
  @JoinColumn(name = "utilisateur_id",foreignKey = @ForeignKey(name="utilisateur_id_fk"))
  private Utilisateur utilisateur;

  public String getNrCompte() {
    return nrCompte;
  }

  public void setNrCompte(String nrCompte) {
    this.nrCompte = nrCompte;
  }

  public String getRib() {
    return rib;
  }

  public void setRib(String rib) {
    this.rib = rib;
  }

  public BigDecimal getSolde() {
    return solde;
  }

  public void setSolde(BigDecimal solde) {
    this.solde = solde;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

}
