package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "VERSEMENT")
public class Versement {

  @Id
  @SequenceGenerator(
			name = "versement_sequence",
			sequenceName = "versement_sequence",
			allocationSize = 1
	)
	
	@GeneratedValue(
			strategy = GenerationType.SEQUENCE,
			generator = "versement_sequence"
	)
  private Long id;
  

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantVirement;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution; 

  @Column
  private String nomEmetteur;

  @Column
  private String prenomEmetteur;

 
  

  @ManyToOne
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifVersement;

  public BigDecimal getMontantVirement() {
    return montantVirement;
  }

  public void setMontantVirement(BigDecimal montantVirement) {
    this.montantVirement = montantVirement;
  }

  public Date getDateExecution() {
    return dateExecution;
  }

  public void setDateExecution(Date dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Compte getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifVersement() {
    return motifVersement;
  }

  public void setMotifVersement(String motifVirement) {
    this.motifVersement = motifVirement;
  }

  public Long getId() {
    return id;
  }

  public String getNomEmetteur() {
    return this.nomEmetteur;
  }

  public void setNomEmetteur(String nomEmetteur) {
    this.nomEmetteur = nomEmetteur;
  }

  public String getPrenomEmetteur() {
    return this.prenomEmetteur;
  }

  public void setPrenomEmetteur(String prenomEmetteur) {
    this.prenomEmetteur = prenomEmetteur;
  }
  
}
