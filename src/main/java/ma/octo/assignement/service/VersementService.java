package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

public interface VersementService {
    public VersementDto  versement(VersementDto versementDto) throws CompteNonExistantException;
}
