package ma.octo.assignement.service;

public interface AuditService {
    public void auditVirement(String message);
    public void auditVersement(String message);
}
