package ma.octo.assignement.requests;

import java.math.BigDecimal;

public class VersementRequest {
    private BigDecimal montantVirement;
    private String nomEmetteur;
    private String prenomEmetteur;
    private String rib;
    private String motifVersement;

    public BigDecimal getMontantVirement() {
        return this.montantVirement;
    }

    public void setMontantVirement(BigDecimal montantVirement) {
        this.montantVirement = montantVirement;
    }

    public String getNomEmetteur() {
        return this.nomEmetteur;
    }

    public void setNomEmetteur(String nomEmetteur) {
        this.nomEmetteur = nomEmetteur;
    }

    public String getRib() {
        return this.rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getMotifVersement() {
        return this.motifVersement;
    }

    public void setMotifVersement(String motifVersement) {
        this.motifVersement = motifVersement;
    }

    public String getPrenomEmetteur() {
        return this.prenomEmetteur;
    }

    public void setPrenomEmetteur(String prenomEmetteur) {
        this.prenomEmetteur = prenomEmetteur;
    }


}
