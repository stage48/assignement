package ma.octo.assignement.serviceImp;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
// import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.VersementService;

@Service
public class VersementServiceImp implements VersementService {
    @Autowired
    CompteRepository compteRepository;

    @Autowired
    VersementRepository versementRepository;

    @Override
    public VersementDto versement(VersementDto versementDto) throws CompteNonExistantException {
       Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRib());
       if(compteBeneficiaire == null) throw new CompteNonExistantException();
       versementDto.setDateExecution(new Date());
       Versement versement = new Versement();
       BeanUtils.copyProperties(versementDto,versement);
       
       BigDecimal newSolde = compteBeneficiaire.getSolde();
       compteBeneficiaire.setSolde(newSolde.add(versement.getMontantVirement()));
       
       versement.setCompteBeneficiaire(compteBeneficiaire);
       versementRepository.save(versement);
       return versementDto;
    }
}
