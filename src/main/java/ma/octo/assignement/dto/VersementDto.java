package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
    
    private BigDecimal montantVirement;
    private Date dateExecution;
    private String nomEmetteur;
    private String prenomEmetteur;
    private String rib;
    private String motifVersement;


    public String getNomEmetteur() {
        return this.nomEmetteur;
    }

    public void setNomEmetteur(String nomEmetteur) {
        this.nomEmetteur = nomEmetteur;
    }


    public BigDecimal getMontantVirement() {
        return this.montantVirement;
    }

    public void setMontantVirement(BigDecimal montantVirement) {
        this.montantVirement = montantVirement;
    }

    public Date getDateExecution() {
        return this.dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

   
    public String getRib() {
        return this.rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getMotifVersement() {
        return this.motifVersement;
    }

    public void setMotifVersement(String motifVersement) {
        this.motifVersement = motifVersement;
    }
    
    public String getPrenomEmetteur() {
        return this.prenomEmetteur;
    }

    public void setPrenomEmetteur(String prenomEmetteur) {
        this.prenomEmetteur = prenomEmetteur;
    }

}
