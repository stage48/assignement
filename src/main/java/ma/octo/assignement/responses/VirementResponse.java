package ma.octo.assignement.responses;

import java.math.BigDecimal;

public class VirementResponse {
    private String nrCompteEmetteur;
    private String nrCompteBeneficiaire;
    private String motif;
    private BigDecimal montantVirement;

    public String getNrCompteEmetteur() {
        return this.nrCompteEmetteur;
    }

    public void setNrCompteEmetteur(String nrCompteEmetteur) {
        this.nrCompteEmetteur = nrCompteEmetteur;
    }

    public String getNrCompteBeneficiaire() {
        return this.nrCompteBeneficiaire;
    }

    public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    }

    public String getMotif() {
        return this.motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public BigDecimal getMontantVirement() {
        return this.montantVirement;
    }

    public void setMontantVirement(BigDecimal montantVirement) {
        this.montantVirement = montantVirement;
    }


}
