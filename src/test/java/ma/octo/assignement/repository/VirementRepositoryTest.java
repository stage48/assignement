package ma.octo.assignement.repository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;

  @Autowired
  private CompteRepository compteRepository;

  @Test
  public void findOne() {
    Optional<Virement> virement = virementRepository.findById(1L);
    Assertions.assertThat(virement.get().getId()).isEqualTo(1L);
  }

  @Test
  public void findAll() {
    List<Virement> virements = virementRepository.findAll();
    Assertions.assertThat(virements.size()).isGreaterThan(0);
  }

  @Test
  public void save() {
    
    Compte compte1 = compteRepository.findByNrCompte("010000A000001000");
    Compte compte2 = compteRepository.findByNrCompte("010000B025001000");

    Virement v = new Virement();
		v.setMontantVirement(BigDecimal.ONE);
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotifVirement("Assignment 2021");

		virementRepository.save(v);
    Assertions.assertThat(v.getId()).isGreaterThan(0);

  }

  @Test
  public void delete() {
    Virement virement = virementRepository.findById(1L).get();
    virementRepository.deleteById(virement.getId());
    Virement virement2 = null ;

    Optional<Virement> Optionalvirement = virementRepository.findById(virement.getId());
    if(Optionalvirement.isPresent()){
      virement2 = Optionalvirement.get();
    }
    Assertions.assertThat(virement2).isNull();
  }
}